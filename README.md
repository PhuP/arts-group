# Fashions Block²

Inspire fashion designers using pretrained models of the [Official PyTorch implementation](https://github.com/SKTBrain/DiscoGAN) of [Learning to Discover Cross-Domain Relations
with Generative Adversarial Networks](https://arxiv.org/pdf/1703.05192.pdf) with a website as a GUI.


The Web-GUI consists of two functions:

With the upload function it is possible to upload an image first and then the category of the uploaded image is chosen. After confirming you can see the translated image in nine different iteration steps.
You can then either change the batchsize of the used model by using the variety slider or converting the image back to the input category by the "Swap categories" button.

upload input screen          |upload result screen
:-------------------------:|:-------------------------:
<img src="assets/screenshots/screen_upload.png" width="1000px">  |  <img src="assets/screenshots/upload_result.png" width="1000px">


The browse function does not need an uploaded image to work. The input and output category have to be chosen and nine random images of the input category from our image pool (/browse_images)
gets translated to the output category. Then in addition to the batchsize, the iteration steps of the model can be changed by the "smartness" slider. 

browse input screen          | browse result screen
:-------------------------:|:-------------------------:
<img src="assets/screenshots/browse_screen.png" width="1000px">  |  <img src="assets/screenshots/browse_result.png" width="1000px">



Prerequisites
------------------------------

- CUDA
- Flask==1.0.2
- numpy==1.15.4
- opencv-python==4.0.0.21
- pandas==0.23.4
- Pillow==5.4.1
- scipy==1.2.0
- torch==1.0.0

Datasets
------------------------------------------------

Following datasets were used to train the models:

 - [Stanford Dogs Dataset](http://vision.stanford.edu/aditya86/ImageNetDogs/)
 - [edges2shoes](https://people.eecs.berkeley.edu/~tinghuiz/projects/pix2pix/datasets/)
 - [edges2handbags](https://people.eecs.berkeley.edu/~tinghuiz/projects/pix2pix/datasets/)
 - [Fruits-360: A dataset of images containing fruits](https://github.com/Horea94/Fruit-Images-Dataset)
 - [Food 524 Database](http://www.ivl.disco.unimib.it/activities/food524db/)


Models
----------------------------------------------------------------------
It is highly recommended to train your own models with [Official PyTorch implementation](https://github.com/SKTBrain/DiscoGAN), because the quality of our models is not very good.
If it is desired to still use our models, they can be downloaded [here](https://box.fu-berlin.de/s/7468PAqXXrxmaQR) and extracted into the project or by just running the `download_models.sh`. 
A lot of model files are missing, because the files were damaged. The .tar.xz archive contains a text file with all the models missing.






Start the Website
----------------

    $ python FBB-Website/app.py 
