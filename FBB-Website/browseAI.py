from PIL import Image, ImageDraw
from torch.autograd import Variable
import cv2
import numpy as np
import torch
import uuid
import os
import glob
import random
from model import *

ai_output_path = "static/ai_results/"

browseimages_path = "../browse_images/"
model_path = "../models/"


def ai(category1, category2, iterations, batchsize):
    """Generates nine images out of random images of the input category """
    browse_images = browseimages_path + category1

    print(browse_images)
    all_images = glob.glob(browse_images + "/*.jpg")
    test_list = random.sample(all_images, 9)
    counter = 1
    for image in test_list:
        calculated_image = calc_image(image, category1, category2, iterations, batchsize)
        input_img = Image.open(image)
        img = Image.open(calculated_image)
        img = img.resize((240, 240))
        input_img.save('static/output_folder/browse_input' + str(counter) + '.png')
        img.save('static/output_folder/browse' + str(counter) + '.png')
        counter += 1


def calc_image(image_path, category1, category2, iteration, batchsize):
    """Generates a single image"""

    cuda = True
    
    possible_dir1 = "/" + category1 + "2" + category2 + "/"
    possible_dir2 = "/" + category2 + "2" + category1 + "/"

    cat1_first = False
    cat2_first = False

    complete_model_path = model_path + "batchsize_" + str(batchsize)

    print(complete_model_path + possible_dir1)

    if os.path.exists(complete_model_path + possible_dir1):
        complete_model_path = complete_model_path + possible_dir1
        cat1_first = True

    if os.path.exists(complete_model_path + possible_dir2):
        complete_model_path = complete_model_path + possible_dir2
        cat2_first = True

    """
    if (category1 == "shoes") != (category2 == "shoes"):
        complete_model_path = complete_model_path + "/" + "something2shoes"
    elif (category1 == "handbags") != (category2 == "handbags"):
        complete_model_path = complete_model_path + "/" + "something2handbags"
    elif (category1 == "handbags" and category2 == "shoes") or (category1 == "shoes" and category2 == "handbags"):
        complete_model_path = complete_model_path + "/" + "handbags2shoes"
    else:
        complete_model_path = complete_model_path + "/" + "cat12cat2"
    
    complete_model_path = complete_model_path + "/discogan/"
    """

    if cat1_first:
        complete_model_path_iter = complete_model_path + "model_state_dict_gen_B-" + str(iteration)
    elif cat2_first:
        complete_model_path_iter = complete_model_path + "model_state_dict_gen_A-" + str(iteration)

    print(complete_model_path)

    image = cv2.resize(cv2.imread(image_path), (64, 64))
    image = image.astype(np.float32) / 255.
    image = [image.transpose(2, 0, 1)]

    test_tensor = Variable(torch.FloatTensor(image))

    if cuda:
        device = torch.device("cuda")
        # model_1 = torch.load(complete_model_path_iter)
        print("load_model")
        model_1 = Generator()
        model_1.load_state_dict(torch.load(complete_model_path_iter))
        model_1.to(device)
    if not cuda:
        device = torch.device("cpu")
        model_1 = Generator()
        model_1.load_state_dict(torch.load(complete_model_path_iter, map_location=device))

    model_1.eval()
    print("model loaded")





    result = model_1(test_tensor.to(device))
    A_val = result[0].cpu().data.numpy().transpose(1, 2, 0) * 255.
    final_img = cv2.cvtColor(A_val.astype(np.uint8)[:, :, ::-1], cv2.COLOR_RGB2BGR)
    final_img = cv2.resize(final_img, (240, 240))

    unique_filename = str(uuid.uuid4())
    final_path = ai_output_path + unique_filename + ".jpg"

    cv2.imwrite(final_path, final_img)

    print("image written")

    return final_path
