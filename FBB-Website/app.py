from flask import Flask, render_template, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
import inputAI
import browseAI

UPLOAD_FOLDER = "static/upload_folder"
ALLOWED_EXTENSIONS = ['png', 'jpg']

app = Flask(__name__)
app.secret_key = b'1'


@app.route('/')
@app.route('/home')
def home():
    return render_template('home.html')


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


picname = ""
category1 = ""
category2 = ""
iterations = 9000
batchsize = 64
swapped = False

iterations_list = [0, 20, 60, 100, 500, 1000, 3000, 6000, 9000]
batchsize_list = [1, 32, 64]


@app.route('/input', methods=['GET', 'POST'])
def input():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('Upload a picture first', 'error')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file', 'error')
            return redirect(request.url)
        if file and allowed_file(file.filename):

            filename = secure_filename(file.filename)
            file.save(UPLOAD_FOLDER + '/' + filename)

            if request.form['select-category-1'] == request.form['select-category-2']:
                flash('Select different categories', 'error')
                return redirect(url_for('input'))
                # return redirect(request.url)

            global picname
            global category1
            global category2
            global iterations
            global batchsize
            picname = filename
            category1 = request.form['select-category-1']
            category2 = request.form['select-category-2']
            batchsize = 64

            arguments = [filename, request.form['select-category-1'], request.form['select-category-2'], 10, 64]
            for i in arguments:
                print(i)
            inputAI.ai(picname, category1, category2, iterations_list, batchsize)

            return redirect(url_for('output'))
    return render_template('input.html')


@app.route('/output', methods=['GET', 'POST'])
def output():
    if request.method == 'POST':
        global picname
        global category1
        global category2
        global iterations
        global batchsize

        # iterations = request.form['iterations']
        batchsize = request.form['batchsize']

        batchsize_for_ai = batchsize_list[int(batchsize)]
        print(batchsize)

        # arguments = [picname, category1, category2, iterations, batchsize]
        # for i in arguments:
        #     print(i)

        inputAI.ai(picname, category1, category2, iterations_list, batchsize_for_ai)

        return redirect(url_for('output'))

    return render_template('output.html', picname=picname, iterations=iterations, batchsize=batchsize)


@app.route('/swap', methods=['GET', 'POST'])
def swap():
    global picname
    global category1
    global category2
    global iterations
    global batchsize
    global swapped

    batchsize_for_ai = batchsize_list[int(batchsize)]

    if swapped == False:
        categoryTemp = category1
        category1 = category2
        category2 = categoryTemp
        swapped = True

    inputAI.ai(picname, category1, category2, iterations_list, batchsize_for_ai)

    return render_template('output.html', picname=picname, iterations=iterations, batchsize=batchsize)


@app.route('/browse_settings', methods=['GET', 'POST'])
def browse_settings():
    global iterations
    global batchsize
    if request.method == 'POST':

        if request.form['select-category-1'] == request.form['select-category-2']:
            flash('Select different categories', 'error')
            return redirect(request.url)

        global category1
        global category2
        category1 = request.form['select-category-1']
        category2 = request.form['select-category-2']
        iterations = 6000
        batchsize = 64

        arguments = [picname, category1, category2, iterations, batchsize]
        for i in arguments:
            print(i)

            browseAI.ai(category1, category2, iterations, batchsize)

        return redirect(url_for('browse_results'))

    iterations = 6
    batchsize = 2

    return render_template('browse_settings.html')


@app.route('/browse_results', methods=['GET', 'POST'])
def browse_results():
    if request.method == 'POST':
        global category1
        global category2
        global iterations
        global batchsize

        iterations = request.form['iterations']
        batchsize = request.form['batchsize']
        print(iterations)
        iterations_for_ai = iterations_list[int(iterations)]
        batchsize_for_ai = batchsize_list[int(batchsize)]

        arguments = [picname, category1, category2, iterations, batchsize]
        for i in arguments:
            print(i)

        browseAI.ai(category1, category2, iterations_for_ai, batchsize_for_ai)

    return render_template('browse_results.html', iterations=iterations, batchsize=batchsize)


if __name__ == '__main__':
    app.run(debug=True)


def debug():
    arguments = [picname, category1, category2, iterations, batchsize]
    for i in arguments:
        print(i)
