import os
import cv2
import numpy as np
import pandas as pd
# from scipy.misc import imresize
import scipy.io


dataset_path = '/media/phu/Elements7/UNI/FU_DGAN'
celebA_path = os.path.join(dataset_path, 'celebA')
handbag_path = os.path.join(dataset_path, 'edges2handbags_small')
shoe_path = os.path.join(dataset_path, 'edges2shoes_small')
facescrub_path = os.path.join(dataset_path, 'facescrub')
chair_path = os.path.join(dataset_path, 'rendered_chairs')
face_3d_path = os.path.join(dataset_path, 'PublicMM1', '05_renderings')
face_real_path = os.path.join(dataset_path, 'real_face')
car_path = os.path.join(dataset_path, 'data', 'cars')

dog_dataset = "dogs_small_new"
food_dataset = "food_small_new"
fruit_dataset = "fruits_test_new"

fruit_path = os.path.join(dataset_path, fruit_dataset)
food_path = os.path.join(dataset_path, food_dataset)
dog_path = os.path.join(dataset_path, dog_dataset)


def shuffle_data(da, db):
    a_idx = range(len(da))
    np.random.shuffle( a_idx )

    b_idx = range(len(db))
    np.random.shuffle(b_idx)

    shuffled_da = np.array(da)[ np.array(a_idx) ]
    shuffled_db = np.array(db)[ np.array(b_idx) ]

    return shuffled_da, shuffled_db

def read_images( filenames, domain=None, image_size=64):

    images = []
    for fn in filenames:
        image = cv2.imread(fn)
        if image is None:
            continue

        if domain == 'A':
            kernel = np.ones((3,3), np.uint8)
            image = image[:, :256, :]
            image = 255. - image
            image = cv2.dilate( image, kernel, iterations=1 )
            image = 255. - image
        elif domain == 'B':
            image = image[:, 256:, :]


        image = cv2.resize(image, (image_size,image_size))
        image = image.astype(np.float32) / 255.
        image = image.transpose(2,0,1)
        images.append( image )


    images = np.stack( images )
    return images

def read_attr_file( attr_path, image_dir ):
    f = open( attr_path )
    lines = f.readlines()
    lines = map(lambda line: line.strip(), lines)
    columns = ['image_path'] + lines[1].split()
    lines = lines[2:]

    items = map(lambda line: line.split(), lines)
    df = pd.DataFrame( items, columns=columns )
    df['image_path'] = df['image_path'].map( lambda x: os.path.join( image_dir, x ) )

    return df




def get_edge2photo_files(item='edges2handbags', test=False):
    if item == 'edges2handbags':
        item_path = handbag_path
    elif item == 'edges2shoes':
        item_path = shoe_path
    elif item == 'dogs':
        item_path = dog_path
    elif item == 'fruits':
        item_path = fruit_path
    elif item == 'food':
        item_path = food_path

    if test == True:
        item_path = os.path.join( item_path, 'val' )
    else:
        item_path = os.path.join( item_path, 'train' )

    image_paths = map(lambda x: os.path.join( item_path, x ), os.listdir( item_path ))

    if test == True:
        return [image_paths, image_paths]
    else:
        n_images = len( image_paths )
        return [image_paths[:n_images/2], image_paths[n_images/2:]]



